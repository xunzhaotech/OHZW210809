![输入图片说明](https://images.gitee.com/uploads/images/2021/0812/133912_b4e13113_1850385.png "征文活动仓库banner1100_90.png")


#  🎉活动意义

#### 「OpenHarmony组件开发大赛」，旨在鼓励让更多开发者了解 OpenHarmony ，将  OpenHarmony  真正玩转起来！

#### 本次大赛提供必要的学习文档和视频直播辅导，同时我们希望通过举办“有奖征文”活动，邀请各位大神和所有参赛者一同分享OpenHarmony相关经验，以帮助大赛参赛者和更多开发者，能够更加全面的了解OpenHarmony。

#### 大家赶快来参加「OpenHarmony组件开发大赛|有奖征文」活动吧！参与投稿还有千元现金奖励、开发板等奖励等你哦！

#### [OpenHarmony组件开发大赛>>>](https://gitee.com/gitee-community/openharmony_components)


#  🎊活动概述

1、 活动形式：有奖征文

2、 面向人群：所有对OpenHarmony感兴趣的开发人员、本次报名参赛的所有选手

3、 征文内容方向：源码解读、OpenHarmony 实践经验、大赛组件开发经验分享

#  📆活动时间

|赛程安排|时间|
|---|---|
|提交时间|8月11日-10月20日|
|评审时间|10月21日-11月03日|
|结果公布|11月08日-11月12日|


#  🎁活动奖励

 **1、悬赏奖金 1000 元（3名）** 

参与投稿，就有机会领取 1000 元悬赏奖金。

> 活动组委会将评出最优秀的文章，发放奖金，各方向1名。
> 
> 🌟注： 以上全部奖金均为税前金额，由主办方代扣代缴个人所得税

 **2、优秀作品奖（10名）** 

除前3名外，官方将在所有投稿的文章中，评选出 10 篇优秀文章（最终数量将根据投递文章确定），为文章作者送出开发板。

 **3、积极投稿奖（30名）** 

所有参与投稿并通过审核的稿件，有机会获得周边T恤一件。


#  📝报名流程和参与方式

1、通过仓库Issues，进行作品提交；

![输入图片说明](https://images.gitee.com/uploads/images/2021/0918/182244_140d8f6b_9459537.png "屏幕截图.png")

2、提交Issues命名格式为：参赛方向（源码解读/实践经验/大赛经验）+ 作品名称

![命名方式](https://images.gitee.com/uploads/images/2021/0918/180549_9b6d1862_9459537.png "屏幕截图.png")

3、提交后，进行登记，方便获奖后工作人员联系您

[已提交，去登记>>>](https://jinshuju.net/f/JEJ9kv)

#  📌征文要求

1、命名要求：请给文件取一个有意义的文件名，文件名长度不得超过 20 个字符（不含扩展名）；

2、每人允许提交多个作品，但单个作品不可以参加多个方向评选；

3、文章字数要求：不少于 500 字；

4、文章要求逻辑清晰、行文流畅、内容完整，图文并茂最佳。


#  🚨注意事项

1、文章须为原创文章；

2、已在参与其他社区征文活动的文章将不计入获奖范围内；

3、需要你注意的是，本次活动将不收录下面几种文章：

- 翻译类文章
- 有失中立性、公正性的内容
- 广告、灌水文章
- 内容与活动主题不符、非原创内容

4、有洗稿、营销软文、广告、抄袭嫌疑的文章一经发现，取消参赛资格，并会影响到之后活动的参加资格；

5、获奖作品、著作权归作者所有， OpenHarmony组件开发大赛工作组拥有使用权。
